import React, {useState} from 'react';
import './todo-list.scss'

interface ITodoList{
    list:IItem[]
    onRemove: (id:number)=>void
    onChecked: (id:number)=>void
    onSort: (sort:boolean|string)=>void
    onClear:()=>void
    itemsLeft:number
    selectedSort:string|boolean
}
export interface IItem{
    text:string,
    isComplete:boolean,
    id:number
}

const TodoList = (props:ITodoList) => {

    return (
        <div className='list'>
            <div className='list__border'>
                <div className="list__items">
                    {props.list.length ? (props.list.map((item,index:number) => (
                        <div key={item.id} className='list__item flex'>
                            <div className='list__title'>
                                <span>{index}.</span>
                                <label className='container'>{item.text}
                                    <input onChange={()=>props.onChecked(item.id)} className='list__checkbox' type="checkbox" checked={item.isComplete}/>
                                    <span className='checkmark'/>
                                </label>
                            </div>
                            <button onClick={()=>props.onRemove(item.id)}/>
                        </div>
                    ))) : <div className='list__empty'><h2>there are no items of this type</h2></div>
                    }
                </div>
                <div className="info flex">
                    <div className="info__left">
                        <span>{props.itemsLeft} items left</span>
                    </div>
                    <div className="info__sort">
                        <button
                            className={`info__btn ${props.selectedSort==='all'&&'clicked'}`}
                            onClick={() => props.onSort('all')}
                        >
                            All
                        </button>
                        <button
                            className={`info__btn ${props.selectedSort===false&&'clicked'}`}
                            onClick={() => props.onSort(false)}
                        >
                            Active
                        </button>
                        <button
                            className={`info__btn ${props.selectedSort===true&&'clicked'}`}
                            onClick={() => props.onSort(true)}
                        >
                            Completed
                        </button>
                    </div>
                    <div className="info__clear">
                        <button onClick={() => props.onClear()}>Clear completed</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TodoList;
