import React, {useState, useRef, useMemo, KeyboardEventHandler} from 'react';
import './todo-input.scss'
import TodoList from "../todo-list/todo-list";
import {useClickOutside} from "../../hooks/use-click-outside";



const TodoInput = () => {
    const[show,setShow]=useState(false)
    const[text, setText]=useState('')
    const[selectedSort,setSelectedSort]=useState<string|boolean>('all')
    const [list,setList] = useState([
        {
            text:'todo1',
            isComplete:false,
            id:1,
        },
        {
            text:'todo2',
            isComplete:false,
            id:2
        },
        {
            text:'todo3',
            isComplete:true,
            id:3
        }
    ])
    const [sortedList,setSortedList]=useState(list)
    const [countActive, setCountActive] = useState(0)

    const onFocus = () => setShow(true)

    const ref1 = useRef<HTMLInputElement>(null);

    useClickOutside(()=>{setShow(false)},ref1)

    const handleKeyDown:KeyboardEventHandler<HTMLInputElement> = (event) => {
        if(event.key=='Enter'){
            addItem()
        }
    }
    const removeItem = (id:number) => {
        setList(list.filter(i=>{
            return i.id !== id}))
    }
    const addItem = () => {
        const item = {text:text,isComplete:false,id:Date.now()}
        if(text){
            setList([item,...list])
            setText('')
        }
    }
    const doneItem = (id:number) => {
        setList(list.map(i=>{
            if(i.id === id) {
                i.isComplete=!i.isComplete
            }
            return i}))
    }

    const clearCompletedItems = () => {
        setList(list.filter(i=>!i.isComplete))
    }

    useMemo(() => {
        setCountActive(list.filter(i => !i.isComplete).length)
        if(selectedSort == 'all'){
            setSortedList([...list])
        }
        else{
            setSortedList([...list].filter(i => {
                return i.isComplete==selectedSort
            }))
        }
    },[selectedSort,list])


    return (
        <div className='input'>
            <div ref={ref1}>
                <div className="input__container">
                    <input
                        placeholder='What needs to be done?'
                        value={text}
                        onChange={e => setText(e.target.value)}
                        onKeyDown={handleKeyDown}
                        className='input__box'
                        onFocus={onFocus} type="text"/>
                    <button className='input__btn' onClick={addItem}/>
                </div>
                    <div className={`todo ${show && 'animation'}`}>
                        <TodoList
                            onSort={e=>setSelectedSort(e)}
                            onChecked={e=>doneItem(e)}
                            onRemove={e=>removeItem(e)}
                            onClear = {clearCompletedItems}
                            selectedSort={selectedSort}
                            itemsLeft={countActive}
                            list={sortedList}/>
                    </div>

            </div>

        </div>
    );
};

export default TodoInput;
