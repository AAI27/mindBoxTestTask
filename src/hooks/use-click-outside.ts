import {useEffect, useRef} from "react";

export const useClickOutside = (handler: any,domNode: any) => {

    useEffect(() => {
        let otherHandler = (event: any) => {
            let node = domNode.current as any
            if(!node.contains(event.target)) {
                handler()
            }
        }
        document.addEventListener('mousedown', otherHandler);

        return () => {
            document.removeEventListener('mousedown', otherHandler)
        }
    })
}
