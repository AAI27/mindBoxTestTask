import React from 'react';
import TodoInput from "./components/todo-input/todo-input";
import './App.scss'
function App() {
  return (
    <div className="App">
        <h1>Todos</h1>
        <TodoInput/>
    </div>
  );
}

export default App;
